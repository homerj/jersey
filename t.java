import javax.ws.rs.client.*;
import javax.ws.rs.core.*;


class t {
	public static void main(String[] args) {
		System.out.println("Test");

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(
			System.getProperty("url.target")
		);

		// uri components
		target = target.path("nick");
		target = target.path("u.jsp");

		System.out.println(target);

		Invocation.Builder builder = target.request(MediaType.TEXT_HTML_TYPE);
		Response response = builder.get();
		System.out.println(response.readEntity(String.class));
	}
}
